package com.blog.serviceImpl;

import com.blog.dao.ViewDao;
import com.blog.entity.View;
import com.blog.service.ViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author suoyue_zhan
 * @date 2021/02/08 12:53
 * @descript 访问量接口实现类
 */
@Service
public class ViewServiceImpl implements ViewService {

    @Autowired
    private ViewDao viewDao;

    @Override
    public List<View> selectViewsByDate(String dateFrom, String dateTo) {
        return viewDao.selectViewsByDate(dateFrom,dateTo);
    }

    @Override
    public Integer insertView(View view) {
        return viewDao.insertView(view);
    }

}
