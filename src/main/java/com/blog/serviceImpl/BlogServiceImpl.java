package com.blog.serviceImpl;

import com.blog.NotFoundException;
import com.blog.dao.BlogDao;
import com.blog.entity.Blog;
import com.blog.enums.StaticVariable;
import com.blog.service.BlogService;
import com.blog.util.MarkdownUtils;
import com.blog.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 博客列表业务层接口实现类
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogDao blogDao;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public ShowBlog getBlogById(Long id,Long userId) {
        return blogDao.getBlogById(id,userId);
    }

    @Override
    public List<Blog> getAllBlog() {
        return blogDao.getAllBlog();
    }

    @Override
    public List<Blog> getBlogByAdminSearch(String title, Long userId) {
        return blogDao.getBlogByAdminSearch(title,userId);
    }

    @Override
    public List<BlogQuery> getUserBlog(Long userId) {
        return blogDao.getUserBlog(userId);
    }

    @Override
    public int updateBlogView(Long id, Long userId) {
        return blogDao.updateBlogView(id,userId,new Date());
    }

    @Override
    public int updateBlogInfo(Blog blog) {
        return blogDao.updateBlogInfo(blog);
    }

    @Override
    public List<BlogQuery> getArchiveBlog() {
        return blogDao.getArchiveBlog();
    }

    @Override
    public int saveBlog(Blog blog) {
        //初始化
        blog.setCreateTime(new Date());
        blog.setUpdateTime(new Date());
        blog.setViews(0);
        blog.setCommentCount(0);
        //默认设置文章可见
        blog.setRecommend(true);

        //数据插入成功后返回主键ID:blog.getId()
        int res = blogDao.saveBlog(blog);

        /**
         * 将文章刷新到redis缓存数据
         */
        ValueOperations<String, DetailedBlog> operations = redisTemplate.opsForValue();
        DetailedBlog detailedBlog = blogDao.getDetailedBlog(blog.getId());
        String key = StaticVariable.blogDetail + detailedBlog.getId();
        operations.set(key,detailedBlog);

        return res;
    }

    @Override
    public int updateBlog(ShowBlog showBlog) {
        showBlog.setUpdateTime(new Date());
        int res = blogDao.updateBlog(showBlog);

        /**
         * 更新文章后更新redis缓存数据
         */
        ValueOperations<String, DetailedBlog> operations = redisTemplate.opsForValue();
        DetailedBlog detailedBlog = blogDao.getDetailedBlog(showBlog.getId());
        String key = StaticVariable.blogDetail + detailedBlog.getId();
        operations.set(key,detailedBlog);
        return res;
    }

    @Override
    public void deleteBlog(Long id) {
        blogDao.deleteBlog(id);
    }

    @Override
    public List<BlogQuery> getBlogBySearch(SearchBlog searchBlog) {
        return blogDao.searchByTitleOrTypeOrRecommend(searchBlog);
    }

    @Override
    public List<FirstPageBlog> getAllFirstPageBlog() {
        return blogDao.getFirstPageBlog();
    }

    @Override
    public List<FirstPageBlog> getHotFirstPageBlog() {
        return blogDao.getHotFirstPageBlog();
    }

    @Override
    public List<FirstPageBlog> getNewFirstPageBlog() {
        return blogDao.getNewFirstPageBlog();
    }

    @Override
    public List<RecommendBlog> getRecommendedBlog() {
        String key = "blog_recommand";
        ValueOperations<String, List<RecommendBlog>> operations = redisTemplate.opsForValue();
        List<RecommendBlog> recommendBlogList = operations.get(key);
        if(CollectionUtils.isEmpty(recommendBlogList)){
            recommendBlogList = blogDao.getAllRecommendBlog();
            operations.set(key,recommendBlogList,12,TimeUnit.HOURS);
        }
        return recommendBlogList;
    }

    @Override
    public List<FirstPageBlog> getSearchBlog(String query) {
        return blogDao.getSearchBlog(query);
    }


    @Override
    public DetailedBlog getDetailedBlog(Long id) {
        //缓存key
        String key = StaticVariable.blogDetail +id;
        ValueOperations<String, DetailedBlog> operations = redisTemplate.opsForValue();
        //Redis 用了GET命令
        DetailedBlog detailedBlog = operations.get(key);
        if(detailedBlog == null){
            //若redis没有的话，直接去数据库捞
            detailedBlog = blogDao.getDetailedBlog(id);
            //重新set到redis，方便下次查找
            operations.set(key,detailedBlog);
//            operations.set(key,detailedBlog,1,TimeUnit.DAYS);
            if (detailedBlog == null) {
                throw new NotFoundException("该博客不存在");
            }
        }


        String content = detailedBlog.getContent();
        //复制一份，修改内容时不会修改到数据库
//        Blog b = new Blog();
//        BeanUtils.copyProperties(detailedBlog,b);
        //博客内容格式转换
        detailedBlog.setContent(MarkdownUtils.markdownToHtmlExtensions(content));


        /**
         * 文章访问数量自增
         * DB操作 blogDao.updateViews(id);  -> 改redis缓存＋定时任务持久化到mysql
         */
        ValueOperations<String,Integer> operationsNum = redisTemplate.opsForValue();
        String keyNum = StaticVariable.blogNum+id;
        operationsNum.increment(keyNum);

//        Integer valueNum = operationsNum.get(keyNum);
//        if(valueNum == null){   //将文章阅读量首次加入redis
//            operationsNum.set(keyNum,detailedBlog.getViews());
//        }else {
//            operationsNum.increment(keyNum);
//        }

        detailedBlog.setViews(operationsNum.get(keyNum)+detailedBlog.getViews());

        //文章评论数量更新
//        blogDao.getCommentCountById(id);
        return detailedBlog;
    }

    @Override
    public List<DetailedBlog> getAllDetailedBlog() {
        return blogDao.getAllDetailedBlog();
    }

    @Override
    public List<FirstPageBlog> getByTypeId(Long typeId) {
        return blogDao.getByTypeId(typeId);
    }

    @Override
    public int updateView(Long id, Integer views) {
        return blogDao.updateView(id,views);
    }

    @Override
    public Integer getBlogTotal() {
        return blogDao.getBlogTotal();
    }

    @Override
    public Integer getBlogViewTotal() {
        return blogDao.getBlogViewTotal();
    }

    @Override
    public Integer getBlogCommentTotal() {
        return blogDao.getBlogCommentTotal();
    }

    @Override
    public Integer getBlogMessageTotal() {
        return blogDao.getBlogMessageTotal();
    }

}