package com.blog.serviceImpl;

import com.blog.dao.PictureDao;
import com.blog.entity.Image;
import com.blog.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author zqc
 * @Date 2021/1/18-9:53
 * @Description
 */
@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureDao pictureDao;

    @Override
    public List<Image> selectPictureList(Long userId) {
        return pictureDao.selectPictureList(userId);
    }

    @Override
    public int insertPicture(Image image) {
        return pictureDao.insertPicture(image);
    }

    @Override
    public int deleteImageInfoById(Long id) {
        return pictureDao.deleteImageInfoById(id);
    }

    @Override
    public int updateImageValid(Long id, Long userId) {
        return pictureDao.updateImageValid(id,userId);
    }

    @Override
    public Image selectImageInfo(Long id) {
        return pictureDao.selectImageInfo(id);
    }

    @Override
    public int updateImageInfo(Image image) {
        return pictureDao.updateImageInfo(image);
    }

    @Override
    public String selectURLById(Long id,Long userId) {
        return pictureDao.selectURLById(id,userId);
    }

}
