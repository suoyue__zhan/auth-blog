package com.blog.serviceImpl;

import com.blog.dao.TypeDao;
import com.blog.entity.Type;
import com.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 文章分类业务层接口实现类
 */
@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeDao typeDao;

    @Transactional
    @Override
    public int saveType(Type type) {
        return typeDao.saveType(type);
    }

    @Transactional
    @Override
    public Type getType(Long id) {
        return typeDao.getType(id);
    }

    @Override
    public List<Type> getUserType(Long userId) {
        return typeDao.getUserType(userId);
    }

    @Override
    public List<Type> getAllTypeAndBlog() {
        return typeDao.getAllTypeAndBlog();
    }

    @Override
    public List<Type> getUserTypeAndBlog(Long userId) {
        return typeDao.getUserTypeAndBlog(userId);
    }

    @Override
    public Type getTypeByNameAndUserId(String name, Long userId) {
        return typeDao.getTypeByNameAndUserId(name,userId);
    }

    @Override
    public Type getTypeByNameAndId(String name, Long id) {
        return typeDao.getTypeByNameAndId(name,id);
    }

    @Transactional
    @Override
    public int updateType(Type type) {
        return typeDao.updateType(type);
    }

    @Transactional
    @Override
    public void deleteType(Long id,Long userId) {
        typeDao.deleteType(id,userId);
    }


}
