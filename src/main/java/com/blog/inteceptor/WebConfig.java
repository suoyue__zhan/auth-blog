package com.blog.inteceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 控制器 - 配置信息
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {        //extends WebMvcConfigurationSupport会拦截静态文件，推荐使用implements WebMvcConfigurer
                                                            //WebMvcConfigurationSupport至少需要实现三个方法 preHandle + postHandle + afterCompletion
    //拦截器的配置类-指明拦截的类型
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/admin/**")
                .addPathPatterns("/user/**")
                .excludePathPatterns("/admin")
                .excludePathPatterns("/admin/login");
    }

}
