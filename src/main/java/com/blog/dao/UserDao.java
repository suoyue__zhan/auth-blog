package com.blog.dao;

import com.blog.entity.Role;
import com.blog.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户持久层接口
 */
@Mapper
@Repository
public interface UserDao {

    //用户名＋密码登录
    User findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    //邮箱登录检测
    User selectByEmailAndPassword(@Param("email") String email, @Param("password") String password);

    //通过邮箱查找用户信息
    User selectUserByEmail(@Param("email") String email);

    //插入用户信息
    int insertUserInfo(User user);

    //修改用户信息
    int updateUserInfo(User user);

    //查找可用用户基本信息列表
    List<User> selectUserInfo();

    //查询首页用户信息列表
    List<User> selectFirstPageUser();

    //查找全部用户信息列表
    List<User> selectUserList();

    //动态（多条件-无boolean值）用户信息列表
    List<User> selectDynamicUserInfo(User user);

    //动态（多条件-有boolean值）用户信息列表
    List<User> selectBooleanUserInfo(User user);

    //查找用户基本信息
    User selectUserDetailInfo(@Param("id")Long id);

    //通过ID查找角色信息
    List<Role> getRolesByUid(Long uid);
    //插入用户与角色信息关联表
    int addRoles(@Param("roles") String[] roles, @Param("uid") Long uid);


}
