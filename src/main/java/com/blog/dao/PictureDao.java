package com.blog.dao;

import com.blog.entity.Image;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 图片持久层接口
 */
@Mapper
@Repository
public interface PictureDao {

    //查询用户所有图片信息
    List<Image> selectPictureList(@Param("userId")Long userId);

    //插入图片信息
    int insertPicture(Image image);


    int deleteImageInfoById(Long id);       //删除图片信息

    //设置图片信息不可用
    int updateImageValid(Long id,Long userId);

    //查询图片信息
    Image selectImageInfo(Long id);

    int updateImageInfo(Image image);       //修改图片信息

    //查找图片链接
    String selectURLById(@Param("id")Long id,@Param("userId")Long userId);

}