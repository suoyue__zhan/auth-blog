package com.blog.dao;

import com.blog.entity.Blog;
import com.blog.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 博客持久层接口
 */
@Mapper
@Repository
public interface BlogDao {

    //查看用户文章
    ShowBlog getBlogById(@Param("id") Long id,@Param("userId") Long userId);

    //查询所有文章列表
    List<Blog> getAllBlog();

    //管理员的文章搜索
    List<Blog> getBlogByAdminSearch(@Param("title") String title,@Param("userId") Long userId);

    //获取用户的文章列表
    List<BlogQuery> getUserBlog(@Param("userId") Long userId);

    //设置文章可见性－用户删除文件
    int updateBlogView(@Param("id") Long id, @Param("userId") Long userId, @Param("updateTime") Date updateTime);

    //动态更新文章信息
    int updateBlogInfo(Blog blog);

    //时间轴－最新文章列表
    List<BlogQuery> getArchiveBlog();

    int saveBlog(Blog blog);

    int updateBlog(ShowBlog showBlog);

    void deleteBlog(Long id);

    List<BlogQuery> searchByTitleOrTypeOrRecommend(SearchBlog searchBlog);

    //热门文章列表
    List<FirstPageBlog> getFirstPageBlog();

    //首页的热门文章
    List<FirstPageBlog> getHotFirstPageBlog();

    //首页的最新文章－根据updateTime排序
    List<FirstPageBlog> getNewFirstPageBlog();

    List<RecommendBlog> getAllRecommendBlog();

    List<FirstPageBlog> getSearchBlog(String query);

    DetailedBlog getDetailedBlog(Long id);

    //进redis
    List<DetailedBlog> getAllDetailedBlog();

    int updateView(@Param("id")Long id,@Param("views")Integer views);

    int updateViews(Long id);

//    根据博客id查询出评论数量
    int getCommentCountById(Long id);

    List<FirstPageBlog> getByTypeId(Long typeId);

    Integer getBlogTotal();

    Integer getBlogViewTotal();

    Integer getBlogCommentTotal();

    Integer getBlogMessageTotal();
}