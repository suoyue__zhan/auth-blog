package com.blog.dao;

import com.blog.entity.View;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author suoyue_zhan
 * @date 2021/02/08 11:35
 * @descript 访问量持久层接口
 */
@Repository
@Mapper
public interface ViewDao {

    //查询日期范围的数据
    List<View> selectViewsByDate(@Param("dateFrom") String dateFrom, @Param("dateTo") String dateTo);
    //插入访问量数据
    Integer insertView(View view);
}
