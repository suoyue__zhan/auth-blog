package com.blog.dao;

import com.blog.entity.Type;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文章分类持久层接口
 */
@Mapper
@Repository
public interface TypeDao {

    //新增分类信息
    int saveType(Type type);

    //查询分类页面
    Type getType(Long id);

    //获取用户对应的分类
    List<Type> getUserType(@Param("userId")Long userId);

    List<Type> getAllTypeAndBlog();

    //查询用户拥有的分类信息
    List<Type> getUserTypeAndBlog(@Param("userId")Long userId);

    //根据用户id判断分类是否存在
    Type getTypeByNameAndUserId(@Param("name")String name,@Param("userId")Long userId);
    //根据分类id判断分类是否存在
    Type getTypeByNameAndId(@Param("name")String name,@Param("id")Long id);

    //修改分类信息
    int updateType(Type type);

    //删除分类信息
    void deleteType(@Param("id")Long id,@Param("userId") Long userId);

}
