package com.blog.controller;

import com.blog.entity.User;
import com.blog.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "用户-注册控制器")
@Controller
//@RequestMapping("/user")
public class RegisterController {

    @Autowired
    private UserService userService;

    @ApiOperation("直接请求则直接跳转")
    @GetMapping({"/register"})
    public String registerPage(Model model){
        model.addAttribute("user",new User());
        return "register";
    }

    @ApiOperation("注册检测")
    @PostMapping("/register")
    public String registerInfo(@RequestParam String code, User user, Model model) {
        //判断是获取验证码还是注册
        if(StringUtils.isEmpty(code)){
            int sentRes = userService.checkEmailCode(user.getEmail());
            if(sentRes == 1){
                model.addAttribute("message","邮件发送成功，请注意查收!!! ");     //将信息添加到缓存中，避免重定向数据丢失
            }else {
                model.addAttribute("message","请忽重复发送邮件!!! ");
            }
            model.addAttribute("user",user);
            return "register";
//            return "user/register :: registerInfo";
        }else {
            int res = userService.checkSignInInfo(user,code);
            switch (res){
                case 1:{
                    model.addAttribute("message","注册成功!!! ");
                    return "login";
                }
                case 2:{
                    model.addAttribute("message","请核对密码填写是否有误!!! ");
                    return "register";
                }
                case 3:{
                    model.addAttribute("message","验证码已过期!!! ");
                    return "register";
                }
                case 4:{
                    model.addAttribute("message","验证码有误!!! ");
                    return "register";
                }
                case 5:{
                    model.addAttribute("message","邮箱已注册，请用验证码登录!!! ");
                    return "register";
                }
                default:{
                    model.addAttribute("message","注册失败!!! ");
                    return "redirect:/register";
                }
            }

//            if(res == 1){
//                model.addAttribute("message","注册成功!!! ");     //将信息添加到缓存中，避免重定向数据丢失
//                return "user/login";
//            }else if(res == 2){
//                model.addAttribute("message","请核对密码填写是否有误!!! ");     //将信息添加到缓存中，避免重定向数据丢失
//                return "user/register";
//            }else if(res == 3){
//                model.addAttribute("message","验证码已过期!!! ");     //将信息添加到缓存中，避免重定向数据丢失
//                return "user/register";
//            }else if(res == 4){
//                model.addAttribute("message","验证码有误!!! ");     //将信息添加到缓存中，避免重定向数据丢失
//                return "user/register";
//            }else if(res == 5){
//                model.addAttribute("message","邮箱已注册，请用验证码登录!!! ");     //将信息添加到缓存中，避免重定向数据丢失
//                return "user/register";
//            }else {
//                model.addAttribute("message","注册失败!!! ");     //将信息添加到缓存中，避免重定向数据丢失
//                return "redirect:/user/register";
//            }
        }
    }


}
