//package com.blog.controller;
//
//import com.blog.service.BlogService;
//import com.blog.service.ViewService;
//import com.blog.util.ResultUtil;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.net.URLConnection;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * @author suoyue_zhan
// * @date 2021/02/06 21:55
// * @descript
// */
//@Slf4j
//@Api(tags = "数据统计界面显示控制器")
//@RestController
//public class EChartsController {
//
//    @Autowired
//    private BlogService blogService;
//
//    @Autowired
//    private ViewService viewService;
//
//    @Autowired
//    private RedisTemplate redisTemplate;
//
//    /**
//     * 疫情数据请求获取的URL地址
//     */
//    //开课吧疫情数据
//    private String EPIDEMIC_URL_KAIKEBA = "https://zaixianke.com/yq/info";
//    //开课吧疫情数据-今日新增数据
//    private String EPIDEMIC_URL_KAIKEBA_DATA = "https://zaixianke.com/yq/info?today";
//    //新浪疫情数据
//    private String EPIDEMIC_URL_XINLANG = "https://gwpre.sina.cn/interface/fymap2020_data.json?_=1585053244653";
//    //网易疫情数据
//    private String EPIDEMIC_URL_WANGYI = "https://c.m.163.com/ug/api/wuhan/app/data/list-total?t=317010693490";
//
//    @ApiOperation("数据统计页面调整")
//    @GetMapping("/echarts")
//    public ModelAndView eCharts(ModelAndView modelAndView){
//        modelAndView.setViewName("echarts.html");
//        return modelAndView;
//    }
//
//    @ApiOperation("疫情地图接口数据")
//    @GetMapping("/echart/totaoData")
//    public String eChartsEpidemic(){
//        ResultUtil result = new ResultUtil();
//
//        try {
//            //创建一个网址对象 (统一资源定位符对象)
//            URL url = new URL(EPIDEMIC_URL_KAIKEBA);
//            //打开网络连接,并得到连接对象
//            URLConnection urlConnection = url.openConnection();
//            //通过连接对象 获取 读取页面内容的 输入流(字节流)
//            InputStream inputStream = urlConnection.getInputStream();
//            //将上述的字节流, 转换为字符输入流 (带有缓冲功能,能一次读取一行的字符输入流)
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
////            读取结果乱码处理
////            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream),"UTF-8");
//            return bufferedReader.readLine();
////            return result.success(bufferedReader.readLine());
//        } catch (MalformedURLException e) {
//            log.info("=============获取疫情数据异常============="+e.getMessage());
//        } catch (IOException e) {
//            log.info("=============疫情数据链接建立异常============="+e.getMessage());
//        }
////        return result.failure(ResultCode.SYSTEM_ERROR);
//        return "";
//    }
//
//
//
//    public static void main(String[] args) {
//        long timeMillis = System.currentTimeMillis();
//        Date date = new Date(timeMillis);
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        String endTime = simpleDateFormat.format(date);
//        timeMillis -= 6*24*60*60*1000;
//        date = new Date(timeMillis);
//        String startTime = simpleDateFormat.format(date);
//
//        System.out.println(date);
//        System.out.println(endTime);
//        System.out.println(startTime);
//
//
//        try{
//            //创建一个网址对象 (统一资源定位符对象)
//            URL url = new URL("https://zaixianke.com/yq/info");
//            //打开网络连接,并得到连接对象
//            URLConnection urlConnection = url.openConnection();
//            //通过连接对象 获取 读取页面内容的 输入流(字节流)
//            InputStream inputStream = urlConnection.getInputStream();
//            //将上述的字节流, 转换为字符输入流 (带有缓冲功能,能一次读取一行的字符输入流)
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
////            读取结果乱码处理
////            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream),"UTF-8");
//            //通过字符流,读取一行网页的资源,并将其存储在变量text中
//            String text = bufferedReader.readLine();
//            System.out.println(text);
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
//
//    }
//
//}
