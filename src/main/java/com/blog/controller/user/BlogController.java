package com.blog.controller.user;

import com.blog.entity.Blog;
import com.blog.entity.Type;
import com.blog.entity.User;
import com.blog.service.BlogService;
import com.blog.service.TypeService;
import com.blog.vo.BlogQuery;
import com.blog.vo.SearchBlog;
import com.blog.vo.ShowBlog;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;


@Api(tags = "用户-博客管理控制器")
@Controller
@RequestMapping("/user")
public class BlogController {

    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;

    @ApiOperation("博客列表")
    @GetMapping("/blogs")
    public String blogs(Model model, HttpSession session,
                        @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        User user = (User)session.getAttribute("user");
        Long userId = user.getId();

        String orderBy = "update_time desc";
        PageHelper.startPage(pageNum,8,orderBy);
        List<BlogQuery> list = blogService.getUserBlog(userId);

        List<Type> types = typeService.getUserType(userId);
        model.addAttribute("types",types);
        model.addAttribute("pageInfo",list);

        return "user/blogs";
    }

    @ApiOperation("跳转博客新增页面")
    @GetMapping("/blogs/input")
    public String input(Model model, HttpSession session) {
        User user = (User)session.getAttribute("user");
        model.addAttribute("types",typeService.getUserType(user.getId()));
        model.addAttribute("blog", new Blog());
        return "user/blogs-input";
    }

    @ApiOperation("博客新增")
    @PostMapping("/blogs")
    public String post(Blog blog, RedirectAttributes attributes, HttpSession session){
        User userInfo = (User) session.getAttribute("user");
        blog.setUser(userInfo);

        //设置blog的type
        blog.setType(typeService.getType(blog.getType().getId()));
        //设置blog中typeId属性
        blog.setTypeId(blog.getType().getId());
        //设置用户id
        blog.setUserId(blog.getUser().getId());

        int b = blogService.saveBlog(blog);

        if(b == 0){
            attributes.addFlashAttribute("message", "新增失败");
        }else {
            attributes.addFlashAttribute("message", "新增成功");
        }
        return "redirect:/user/blogs";
    }

    @ApiOperation("跳转编辑修改文章")
    @GetMapping("/blogs/{id}/input")
    public String editInput(@PathVariable Long id, Model model, HttpSession session) {
        User user = (User)session.getAttribute("user");
        ShowBlog blogById = blogService.getBlogById(id,user.getId());
        List<Type> allType = typeService.getUserType(user.getId());
        model.addAttribute("blog", blogById);
        model.addAttribute("types", allType);

        return "user/blogs-input";
    }

    @ApiOperation("编辑修改文章")
    @PostMapping("/blogs/{id}")
    public String editPost(@Valid ShowBlog showBlog,
                           HttpSession session, RedirectAttributes attributes) {
        User user = (User)session.getAttribute("user");
        showBlog.setUserId(user.getId());

        int b = blogService.updateBlog(showBlog);
        if(b ==0){
            attributes.addFlashAttribute("message", "修改失败");
        }else {
            attributes.addFlashAttribute("message", "修改成功");
        }
        return "redirect:/user/blogs";
    }

    @ApiOperation("删除文章-文章不可见")
    @GetMapping("/blogs/{id}/delete")
    public String delete(@PathVariable Long id,
                         HttpSession session, RedirectAttributes attributes) {
        User user = (User)session.getAttribute("user");

        blogService.updateBlogView(id,user.getId());

//        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message", "删除成功");
        return "redirect:/user/blogs";
    }

    @ApiOperation("搜索博客")
    @PostMapping("/blogs/search")
    public String search(SearchBlog searchBlog, Model model,HttpSession session,
                         @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum) {
        User user = (User)session.getAttribute("user");
        searchBlog.setUserId(user.getId());

        PageHelper.startPage(pageNum, 8);
        List<BlogQuery> blogBySearch = blogService.getBlogBySearch(searchBlog);

        model.addAttribute("pageInfo", blogBySearch);
        return "user/blogs :: blogList";       //返回admin/blogs下的blogList片段
    }

}
