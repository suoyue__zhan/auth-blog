package com.blog.controller.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(tags = "用户创作中心")
@Controller
@RequestMapping("/user")
public class IndexUserController {

	@ApiOperation("跳转个人中心")
	@GetMapping("/index")
	public String getUserIndex(){
		return "user/index";
	}


}
