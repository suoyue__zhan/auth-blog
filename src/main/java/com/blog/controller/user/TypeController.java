package com.blog.controller.user;

import com.blog.entity.Type;
import com.blog.entity.User;
import com.blog.service.TypeService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "用户-分类管理控制器")
@Controller
@RequestMapping("/user")
public class TypeController {

    @Autowired
    private TypeService typeService;

    @ApiOperation("分页查询分类列表")
    @GetMapping("/types")
    public String list(Model model, HttpSession session,
                       @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        User user = (User)session.getAttribute("user");
        //按照排序字段 倒序 排序
        String orderBy = "id desc";
        PageHelper.startPage(pageNum,8,orderBy);
        List<Type> list = typeService.getUserType(user.getId());
        model.addAttribute("pageInfo",list);
        return "user/types";
    }

    @ApiOperation("返回新增分类页面")
    @GetMapping("/types/input")
    public String input(Model model){
        model.addAttribute("type", new Type());
        return "user/types-input";
    }

    @ApiOperation("新增分类")
    @PostMapping("/types")
    public String post(@Valid Type type,
                       HttpSession session,RedirectAttributes attributes) {
        User user = (User)session.getAttribute("user");
        Long uid = user.getId();
        Type type1 = typeService.getTypeByNameAndUserId(type.getName(),uid);
        if (type1 != null) {
            attributes.addFlashAttribute("message", "不能添加重复的分类");
            return "redirect:/user/types/input";
        }

        type.setUserId(uid);
        int t = typeService.saveType(type);
        if (t == 0) {
            attributes.addFlashAttribute("message", "新增失败");
        } else {
            attributes.addFlashAttribute("message", "新增成功");
        }
        return "redirect:/user/types";
    }

    @ApiOperation("跳转修改分类页面")
    @GetMapping("/types/{id}/input")
    public String editInput(@PathVariable Long id, Model model) {
        model.addAttribute("type", typeService.getType(id));
        return "user/types-input";
    }

    @ApiOperation("编辑修改分类")
    @PostMapping("/types/{id}")
    public String editPost(@Valid Type type, @PathVariable Long id, RedirectAttributes attributes) {
        Type type1 = typeService.getTypeByNameAndId(type.getName(),id);
        if (type1 != null) {
            attributes.addFlashAttribute("message", "不能添加重复的分类");
            return "redirect:/user/types/input";
        }
        int t = typeService.updateType(type);       //直接传入实体类信息，包括id
        if (t == 0 ) {
            attributes.addFlashAttribute("message", "更新失败");
        } else {
            attributes.addFlashAttribute("message", "更新成功");
        }
        return "redirect:/user/types";
    }

    @ApiOperation("删除分类")
    @GetMapping("/types/{id}/delete")
    public String delete(@PathVariable Long id, HttpSession session,
                         RedirectAttributes attributes) {
        User user = (User)session.getAttribute("user");
        typeService.deleteType(id,user.getId());
        attributes.addFlashAttribute("message", "删除成功");
        return "redirect:/user/types";
    }

}
