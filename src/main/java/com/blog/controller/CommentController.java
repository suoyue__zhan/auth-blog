package com.blog.controller;

import com.blog.entity.Comment;
import com.blog.entity.User;
import com.blog.service.BlogService;
import com.blog.service.CommentService;
import com.blog.vo.DetailedBlog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Api(tags = "评论控制器")
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private BlogService blogService;

    @Value("${comment.avatar}")
    private String avatar;

    @ApiOperation("查询评论列表")
    @GetMapping("/comments/{blogId}")
    public String comments(@PathVariable Long blogId, Model model) {
        List<Comment> comments = commentService.listCommentByBlogId(blogId);
        model.addAttribute("comments", comments);
        return "blog :: commentList";
    }

    @ApiOperation("新增评论")
    @PostMapping("/comments")
    public String post(Comment comment, HttpSession session,Model model) {
        Long blogId = comment.getBlogId();
        User user = (User) session.getAttribute("user");

        //登录后才可评论
        if(user == null){
            return "blog :: commentList";
        }

        comment.setAvatar(user.getAvatar());
        if(comment.getBlogId() == user.getId()){
            comment.setAdminComment(true);
        }else {
            comment.setAdminComment(false);
        }

        if (comment.getParentComment().getId() != null) {
            comment.setParentCommentId(comment.getParentComment().getId());
        }
        commentService.saveComment(comment);
        commentService.deleteRedisBlogAndComment(blogId);

        //留言信息保存好后在发送邮件通知
//        try {
//            if(user == null){
//                MailUtil.textMail("评论人称呼："+comment.getNickname()+" 邮箱："+comment.getEmail(),"评论内容："+comment.getContent(), StaticVariable.receiverMail);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        List<Comment> comments = commentService.listCommentByBlogId(blogId);
        model.addAttribute("comments", comments);

        return "blog :: commentList";
    }

    @ApiOperation("删除评论")
    @GetMapping("/comment/{blogId}/{id}/delete")
    public String delete(@PathVariable Long blogId, @PathVariable Long id,Comment comment, Model model){

        commentService.deleteComment(comment,id);
        commentService.deleteRedisBlogAndComment(blogId);
        DetailedBlog detailedBlog = blogService.getDetailedBlog(blogId);
        List<Comment> comments = commentService.listCommentByBlogId(blogId);
        model.addAttribute("blog", detailedBlog);
        model.addAttribute("comments", comments);
        return "redirect:/blog/{blogId}";
    }

}