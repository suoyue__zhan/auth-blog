package com.blog.controller.admin;

import com.blog.entity.User;
import com.blog.service.UserService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "用户信息管理")
@Slf4j
@Controller
@RequestMapping("/admin")
public class UserAdminController {

	@Autowired
	private UserService userService;

	@ApiOperation("跳转用户信息管理中心")
	@GetMapping("/users")
	public ModelAndView getUserList(ModelAndView modelAndView,
								@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
		PageHelper.startPage(pageNum,8);
		List<User> userList = userService.selectUserList();
		modelAndView.addObject("users",userList);
		modelAndView.setViewName("admin/users.html");
		return modelAndView;
	}

	@ApiOperation("搜索用户信息")
	@PostMapping("/users/search")
	public String search(@RequestParam("username")String username, @RequestParam("enabled")String enabled, Model model,
						 @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum) {

		User user = new User();
		List<User> userList = new ArrayList<>();

		if(!StringUtils.isEmpty(username)){
			user.setUsername(username);
		}

		//enabled布尔值，new对象默认为false，特殊处理
		if(StringUtils.isEmpty(enabled)){
			PageHelper.startPage(pageNum, 8);
			userList = userService.selectDynamicUserInfo(user);
		}else {
			if("1".equals(enabled)){
				user.setEnabled(true);
			}else if("0".equals(enabled)){
				user.setEnabled(false);
			}
			PageHelper.startPage(pageNum, 8);
			userList = userService.selectBooleanUserInfo(user);
		}

		model.addAttribute("users",userList);
		return "admin/users :: userList";       //返回admin/blogs下的blogList片段
	}

	@ApiOperation("禁用用户")
	@GetMapping("/user/disable/{id}/{enabled}")
	public String disableUser(@PathVariable Long id, @PathVariable boolean enabled,
							  RedirectAttributes attributes){
		if(enabled == false){
			attributes.addFlashAttribute("message","该用户已是禁用状态，无需重复禁用...");
		}else {
			User user = new User();
			user.setId(id);
			user.setEnabled(false);
			int res = userService.updateUserInfo(user);
			if(res == 1){
				attributes.addFlashAttribute("message","禁用操作成功...");
			}else {
				attributes.addFlashAttribute("message","禁用操作失败...");
			}
		}
		return "redirect:/admin/users";
	}

	@ApiOperation("解除禁用")
	@GetMapping("/user/release/{id}/{enabled}")
	public String releaseUser(@PathVariable Long id,@PathVariable boolean enabled,
									RedirectAttributes attributes){

		if(enabled == true){
			attributes.addFlashAttribute("message","该用户未被禁用，无需解禁...");
		}else {
			User user = new User();
			user.setId(id);
			user.setEnabled(true);
			int res = userService.updateUserInfo(user);
			if(res == 1){
				attributes.addFlashAttribute("message","解除禁用操作成功...");
			}else {
				attributes.addFlashAttribute("message","解除禁用操作失败...");
			}
		}
		return "redirect:/admin/users";
	}

}
