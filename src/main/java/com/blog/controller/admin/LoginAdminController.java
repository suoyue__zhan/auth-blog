package com.blog.controller.admin;

import com.blog.entity.User;
import com.blog.service.UserService;
import com.wf.captcha.utils.CaptchaUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Api(tags = "后台-登录控制器")
@Controller
@RequestMapping("/admin")
public class LoginAdminController {

    @Autowired
    private UserService userService;

    @ApiOperation("空登录/直接请求则直接跳转")
    @GetMapping({"","login"})
    public String loginPage(){
        //HttpServletRequest request
        //String uri = request.getRequestURI();
        //uri.endsWith("/login")
        return "admin/login";
    }

    @ApiOperation("登录检测")
    @PostMapping("/login")
    public String login(@RequestParam String captcha,
                        @RequestParam String username, @RequestParam String password,
                        HttpSession session, HttpServletRequest request,RedirectAttributes attributes) {

        //先判断验证码
        if (!CaptchaUtil.ver(captcha, request)) {
            CaptchaUtil.clear(request);  // 清除session中的验证码
            attributes.addFlashAttribute("message","验证码有误!!! ");     //将信息添加到缓存中，避免重定向数据丢失
            return "redirect:/admin";
        }

        //DB匹配用户信息
        User user = userService.checkUser(username,password);
        if(user != null){
            if(user.getType()==2){
                //避免同一浏览器多人登录
                session.removeAttribute("admin");
                session.removeAttribute("user");

                session.setAttribute("admin",user);
                session.setAttribute("user",user);
                return "admin/index";
            }else {
                attributes.addFlashAttribute("message","权限不足，请联系管理员!!! ");     //将信息添加到缓存中，避免重定向数据丢失
                return "redirect:/admin";
            }
        }else{
            attributes.addFlashAttribute("message","用户名或密码有误!!! ");     //将信息添加到缓存中，避免重定向数据丢失
            return "redirect:/admin";
        }
    }

    @ApiOperation("退出-注销操作")
    @GetMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute("admin");
        return "redirect:/admin";
    }

}
