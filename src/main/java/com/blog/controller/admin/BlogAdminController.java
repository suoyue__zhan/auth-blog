package com.blog.controller.admin;

import com.blog.entity.Blog;
import com.blog.entity.User;
import com.blog.service.BlogService;
import com.blog.service.UserService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Api(tags = "后台-博客管理控制器")
@Controller
@RequestMapping("/admin")
public class BlogAdminController {

    @Autowired
    private BlogService blogService;
    @Autowired
    private UserService userService;

    @ApiOperation("博客列表")
    @GetMapping("/blogs")
    public String blogs(Model model, HttpServletRequest request,
                        @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        String orderBy = "update_time desc";
        PageHelper.startPage(pageNum,8,orderBy);
        List<Blog> list = blogService.getAllBlog();

        //获取请求URL
        String returnUrl = request.getScheme() + "://" + request.getServerName() + ":"+ request.getServerPort() + request.getContextPath() + "/blog/";
        for(Blog blog : list){
            blog.setUrl(returnUrl+blog.getId());
        }

        //获取用户名
        List<User> userList = userService.selectUserList();
        model.addAttribute("users",userList);
        model.addAttribute("pageInfo",list);

        return "admin/blogs";
    }

    @ApiOperation("搜索博客")
    @PostMapping("/blogs/search")
    public String search(@RequestParam("title") String title, @RequestParam("userId") Long userId,
                         HttpServletRequest request, Model model,
                         @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum) {
        PageHelper.startPage(pageNum, 8);
        List<Blog> blogBySearch = blogService.getBlogByAdminSearch(title,userId);
        //获取请求URL
        String returnUrl = request.getScheme() + "://" + request.getServerName() + ":"+ request.getServerPort() + request.getContextPath() + "/blog/";
        for(Blog blog : blogBySearch){
            blog.setUrl(returnUrl+blog.getId());
        }
        model.addAttribute("pageInfo", blogBySearch);
        return "admin/blogs :: blogList";       //返回admin/blogs下的blogList片段
    }

    @ApiOperation("禁用文章")
    @GetMapping("/blog/disable/{id}/{enabled}")
    public String disableBlog(@PathVariable Long id, @PathVariable boolean enabled,
                              RedirectAttributes attributes){
        if(enabled == false){
            attributes.addFlashAttribute("message","该文章已是屏蔽状态，无需重复屏蔽...");
        }else {
            Blog blog = new Blog();
            blog.setId(id);
            blog.setRecommend(false);
            int res = blogService.updateBlogInfo(blog);
            if(res == 1){
                attributes.addFlashAttribute("message","屏蔽操作成功...");
            }else {
                attributes.addFlashAttribute("message","屏蔽操作失败...");
            }
        }
        return "redirect:/admin/blogs";
    }

    @ApiOperation("解除文章")
    @GetMapping("/blog/release/{id}/{enabled}")
    public String releaseBlog(@PathVariable Long id, @PathVariable boolean enabled,
                              RedirectAttributes attributes){

        if(enabled == true){
            attributes.addFlashAttribute("message","该文章未被屏蔽，无需解禁...");
        }else {
            Blog blog = new Blog();
            blog.setId(id);
            blog.setRecommend(true);
            int res = blogService.updateBlogInfo(blog);
            if(res == 1){
                attributes.addFlashAttribute("message","解除屏蔽操作成功...");
            }else {
                attributes.addFlashAttribute("message","解除屏蔽操作失败...");
            }
        }
        return "redirect:/admin/blogs";
    }

    @ApiOperation("删除文章")
    @GetMapping("/blog/delete/{id}")
    public String deleteBlog(@PathVariable Long id,
                              RedirectAttributes attributes){
        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message","文章删除成功...");
        return "redirect:/admin/blogs";
    }

}
