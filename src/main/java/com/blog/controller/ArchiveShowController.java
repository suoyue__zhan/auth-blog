package com.blog.controller;

import com.blog.enums.ResultCode;
import com.blog.service.BlogService;
import com.blog.util.ResultUtil;
import com.blog.vo.BlogQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Api(tags = "时间轴页面显示控制器")
//@Controller
@RestController
public class ArchiveShowController {

    @Autowired
    private BlogService blogService;

    @ApiOperation("查询所有时间轴信息")
    @GetMapping("/newArticle")
    public ModelAndView archive(ModelAndView modelAndView){
        modelAndView.addObject("blogs",blogService.getArchiveBlog());
        //templates同级目录下，开发环境有无斜杠/均正常，但发到服务器有斜杠访问会报错
        modelAndView.setViewName("newArticle.html");
        return modelAndView;
    }


    /**
     * 测试前后端分离开发的数据封装回显示
     * @return
     */
    @ApiOperation("查询所有时间轴信息")
    @GetMapping("/archivesSwagger")
    public ResultUtil archiveResult(){
        ResultUtil result = new ResultUtil();

        List<BlogQuery> blogs = blogService.getArchiveBlog();
        if(CollectionUtils.isEmpty(blogs)){
            return result.failure(ResultCode.SYSTEM_ERROR);
        }
        return result.success(blogs);
    }


}