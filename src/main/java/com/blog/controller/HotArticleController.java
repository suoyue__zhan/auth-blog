package com.blog.controller;

import com.blog.service.BlogService;
import com.blog.vo.FirstPageBlog;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "热门文章控制器")
@Controller
public class HotArticleController {

    @Autowired
    private BlogService blogService;

    @ApiOperation("分页查询热门文章列表")
    @GetMapping("/hotArticle")
    public String index(Model model, @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,12);
        List<FirstPageBlog> allFirstPageBlog = blogService.getAllFirstPageBlog();       //博文列表

        model.addAttribute("pageInfo",allFirstPageBlog);

        return "hotArticle";
    }

}
