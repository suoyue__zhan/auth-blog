package com.blog.controller;

import com.wf.captcha.GifCaptcha;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author suoyue_zhan
 * @date 2021/01/31 14:49
 * @descript EasyCaptcha - 图形验证码生成
 */

@Controller
public class CaptchaController {

    /**
     * https://gitee.com/whvse/EasyCaptcha
     * CaptchaUtil工具类封装了输出验证码、存session、判断验证码等功能
     * 点击图片刷新
     */
    @RequestMapping("/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //默认验证码
//        CaptchaUtil.out(request, response);

        // 可设置验证码位数
//        CaptchaUtil.out(5, request, response);

        // 可设置验证码宽、高、位数
//        CaptchaUtil.out(130, 48, 5, request, response);

        // 可使用gif验证码
        GifCaptcha gifCaptcha = new GifCaptcha(130,40,4);
        CaptchaUtil.out(gifCaptcha, request, response);
    }

}