package com.blog.controller;

import com.blog.entity.Message;
import com.blog.entity.User;
import com.blog.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

@Api(tags = "留言页面控制器")
@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Value("${message.avatar}")
    private String avatar;

    @GetMapping("/message")
    public String message() {
        return "message";
    }

    @ApiOperation("查询留言")
    @GetMapping("/messagecomment")
    public String messages(Model model) {
        List<Message> messages = messageService.listMessage();
        model.addAttribute("messages", messages);
        return "message::messageList";      //返回给前端的messageList片段 - th:fragment="messageList"
    }


    @ApiOperation("新增留言")
    @PostMapping("/message")
    public String post(Message message, HttpSession session, Model model) {
        User user = (User) session.getAttribute("user");
        //登录后才可留言
        if(user == null){
//            attributes.addFlashAttribute("message","请先登录再发表言论...");
            return "message";
        }

        message.setUserId(user.getId());
        message.setAvatar(user.getAvatar());

        //管理员标准－设置为true
        User admin = (User) session.getAttribute("admin");
        if(admin != null){
            message.setAdminMessage(true);
            message.setUserId(admin.getId());
            message.setAvatar(admin.getAvatar());
        }

        if (message.getParentMessage().getId() != null) {
            message.setParentMessageId(message.getParentMessage().getId());
        }
        messageService.saveMessage(message);

        //留言信息保存好后在发送邮件通知
//        try {
//            if(user == null){
//                MailUtil.textMail("留言人称呼："+message.getNickname()+" 邮箱："+message.getEmail(),"留言内容："+message.getContent(), StaticVariable.receiverMail);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        List<Message> messages = messageService.listMessage();
        model.addAttribute("messages", messages);
        return "message::messageList";
    }

    @ApiOperation("删除留言")
    @GetMapping("/messages/{id}/delete")
    public String delete(@PathVariable Long id, HttpSession session,
                         RedirectAttributes attributes, Model model){
        //管理员：直接删除
        User admin = (User) session.getAttribute("admin");
        if(admin != null){
            messageService.deleteMessage(id);
            model.addAttribute("message","留言删除成功...");
            return "redirect:/message";
        }
        //普通用户：数据库判断后删除
        User user = (User) session.getAttribute("user");
        if(user != null){
            int res = messageService.deleteMessageByUser(id,user.getId());
            if(res == 1){
                model.addAttribute("message","留言删除成功...");
            }else {
                model.addAttribute("message","留言删除失败，权限不足，请联系管理员...");
            }
        }
        return "redirect:/message";
    }

}