package com.blog.controller;

import com.blog.entity.Type;
import com.blog.entity.User;
import com.blog.service.BlogService;
import com.blog.service.TypeService;
import com.blog.service.UserService;
import com.blog.vo.FirstPageBlog;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Api(tags = "好友榜显示控制器")
//@Controller
@RestController
public class ActiveUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private BlogService blogService;

    @ApiOperation("分页查询所有好友信息")
    @GetMapping("/activeUser")
    public ModelAndView getUserInfo(ModelAndView modelAndView,@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum) {
        PageHelper.startPage(pageNum,16);
        List<User> userList =  userService.selectUserInfo();

        modelAndView.addObject("pageInfo",userList);
        modelAndView.setViewName("activeUser.html");
        return modelAndView;
    }

    @ApiOperation("查询用户信息页面")
    @GetMapping("/oneUser/{id}/{uid}")
    public ModelAndView getUserDetailInfo(ModelAndView modelAndView,
                                          @PathVariable("id") Long id,@PathVariable("uid") Long uid,
                                          @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum) {

        //查询用户信息
        User user =  userService.selectUserDetailInfo(uid);
        //查询用户拥有的分类信息
        List<Type> types = typeService.getUserTypeAndBlog(uid);

        //-1表示从首页导航点进来的
        if (id == -1 && types.size()!=0) {
            id = types.get(0).getId();
        }
        PageHelper.startPage(pageNum,8);
        List<FirstPageBlog> blogs = blogService.getByTypeId(id);

        modelAndView.addObject("user",user);
        modelAndView.addObject("types",types);
        modelAndView.addObject("blogs",blogs);
        modelAndView.addObject("activeTypeId", id);

        modelAndView.setViewName("oneUser.html");
        return modelAndView;
    }

}