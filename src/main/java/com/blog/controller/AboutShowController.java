package com.blog.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Api(tags = "关于我界面显示控制器")
@Controller
public class AboutShowController {

    @ApiOperation("测试")
    @GetMapping("/user/about")
    public String aboutUser() {
        return "user/about";
    }


    @ApiOperation("关于我界面显示控制器")
    @GetMapping("/about")
    public String about() {
        return "about";
    }

}