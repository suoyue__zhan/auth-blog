package com.blog.controller;

import com.blog.entity.Comment;
import com.blog.entity.User;
import com.blog.service.BlogService;
import com.blog.service.CommentService;
import com.blog.service.UserService;
import com.blog.vo.DetailedBlog;
import com.blog.vo.FirstPageBlog;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "首页控制器")
@Controller
public class IndexController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;

    @ApiOperation("查询首页活跃用户＋查询首页热门文章＋查询首页最新文章")
    @GetMapping("/")
    public String index(Model model){

        //查询首页热门文章
        List<FirstPageBlog> hotFirstPageBlog = blogService.getHotFirstPageBlog();

        //查询首页最新文章
        List<FirstPageBlog> newFirstPageBlog = blogService.getNewFirstPageBlog();

        //查询首页活跃用户-定义规则 － 重写
        List<User> userList = userService.selectFirstPageUser();

        model.addAttribute("hotPageInfo",hotFirstPageBlog);
        model.addAttribute("newPageInfo",newFirstPageBlog);
        model.addAttribute("userList", userList);

        return "index";
    }

    @ApiOperation("搜索博客-全局搜索－模糊匹配")
    @PostMapping("/search")
    public String search(Model model,
                         @RequestParam(defaultValue = "1", value = "pageNum") Integer pageNum,
                         @RequestParam String query) {

        PageHelper.startPage(pageNum, 1000);
        List<FirstPageBlog> searchBlog = blogService.getSearchBlog(query);
        model.addAttribute("pageInfo", searchBlog);
        model.addAttribute("query", query);

        return "search";
    }

    @ApiOperation("跳转博客详情页面")
    @GetMapping("/blog/{id}")
    public String blog(@PathVariable Long id, Model model) {
        DetailedBlog detailedBlog = blogService.getDetailedBlog(id);
        List<Comment> comments = commentService.listCommentByBlogId(id);

        model.addAttribute("comments", comments);
        model.addAttribute("blog", detailedBlog);
        return "blog";
    }

}
