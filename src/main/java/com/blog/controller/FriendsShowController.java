//package com.blog.controller;
//
//import com.blog.entity.FriendLink;
//import com.blog.enums.ResultCode;
//import com.blog.service.FriendLinkService;
//import com.blog.util.ResultUtil;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.util.List;
//
//@Api(tags = "友链显示控制器")
////@Controller
//@RestController
//public class FriendsShowController {
//
//    @Autowired
//    private FriendLinkService friendLinkService;
//
//    @ApiOperation("查询所有友链信息")
//    @GetMapping("/friends")
//    public ModelAndView friends(ModelAndView modelAndView) {
//        modelAndView.addObject("friendlinks",friendLinkService.listFriendLink());
//        //templates同级目录下，开发环境有无斜杠/均正常，但发到服务器有斜杠访问会报错
//        modelAndView.setViewName("friends.html");
//        return modelAndView;
//    }
//
//    /**
//     * 测试前后端分离开发的数据封装回显示
//     * @param model
//     * @return
//     */
//    @ApiOperation("查询所有友链信息")
//    @GetMapping("/friendsSwagger")
//    public ResultUtil friendsResult(Model model) {
//        ResultUtil result = new ResultUtil();
//
//        try {
//            List<FriendLink> friendLinkList = friendLinkService.listFriendLink();
//            return result.success(friendLinkList);
//        }catch (Exception e){
//            e.printStackTrace();
//            return result.failure(ResultCode.SYSTEM_ERROR);
//        }
//
//    }
//
//}