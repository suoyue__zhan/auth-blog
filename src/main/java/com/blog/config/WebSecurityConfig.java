//package com.blog.config;
//
//import com.blog.serviceImpl.UserServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//
///**
// * @Author zqc
// * @Date 2020/12/24-10:44
// * @Description Security配置类
// */
////@EnableWebSecurity
////@Configuration
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
////    @Autowired
////    private VerifyCodeFilter verifyCodeFilter;
//
////    @Autowired
////    private DataSource dataSource;
//
//    @Autowired
//    UserServiceImpl userService;
//
//    /**
//     * 认证
//     */
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//
//        //使用内存认证 - passwordEncoder:进行密码编码
////        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
////                .withUser("suoyue").password(new BCryptPasswordEncoder().encode("12345")).roles("ADMIN")
////                .and()
////                .withUser("user").password(new BCryptPasswordEncoder().encode("12345")).roles("USER");
//
//        //基于DB认证
////        auth.jdbcAuthentication().dataSource(dataSource);
//
////        auth.userDetailsService(userService).passwordEncoder(new PasswordEncoder() {
////            @Override
////            public String encode(CharSequence password) {
////                return MD5Utils.code((String) password);
////            }
////
////            @Override
////            public boolean matches(CharSequence password, String encodedPassword) {
////                return encodedPassword.equals(MD5Utils.code((String) password));
////            }
////        });
//    }
//
//
//    /**
//     * 授权：配置拦截请求
//     *
//     * anyRequest          |   匹配所有请求路径
//     * access              |   SpringEl表达式结果为true时可以访问
//     * anonymous           |   匿名可以访问
//     * denyAll             |   用户不能访问
//     * fullyAuthenticated  |   用户完全认证可以访问（非remember-me下自动登录）
//     * hasAnyAuthority     |   如果有参数，参数表示权限，则其中任何一个权限可以访问
//     * hasAnyRole          |   如果有参数，参数表示角色，则其中任何一个角色可以访问
//     * hasAuthority        |   如果有参数，参数表示权限，则其权限可以访问
//     * hasIpAddress        |   如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
//     * hasRole             |   如果有参数，参数表示角色，则其角色可以访问
//     * permitAll           |   用户可以任意访问
//     * rememberMe          |   允许通过remember-me登录的用户访问
//     * authenticated       |   用户登录后可访问
//     */
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.headers().frameOptions().sameOrigin();
//        //配置自定义的过滤器
////        http.addFilterBefore(verifyCodeFilter, UsernamePasswordAuthenticationFilter.class);
//        http.authorizeRequests()//返回一个拦截注册器
//                //划分权限 antMatchers：采用ANT模式的URL匹配器
//                .antMatchers("/admin/**").hasRole("ADMIN")  //  /admin/**的URL都需要有超级管理员角色，如果使用.hasAuthority()方法来配置，需要在参数中加上ROLE_,如下.hasAuthority("ROLE_超级管理员")
//                .antMatchers("/user/**").hasRole("USER")    //  /user/**的URL都需要用户登录后才能操作
//                .antMatchers("/").permitAll()
//                .and()      //结束当前标签,衔接下面
//                //使用表单登录
//                .formLogin()
//                // 指定登录页面,授予所有用户访问登录页面
//				.loginPage("/login")
//                .and()
//                .logout()
//                .logoutSuccessUrl("/login").permitAll()
//                .and()
//                // 防止web页面的Frame被拦截
//                .headers().frameOptions().disable();
//
//        //表单登录-登录页面-登录接口-设置访问用户和密码
////        http.formLogin().loginPage("/login.html").loginProcessingUrl("/login").usernameParameter("user").passwordParameter("pwd");
//        //默认HTTP基本认证
////        http.authorizeRequests().antMatchers("/**").fullyAuthenticated().and().httpBasic();
//
//        //跨站请求伪造防护功能，重写方法默认启动   disable:关闭csrf
//        http.csrf().disable();
//
//        //开启注销功能-注销成功跳完首页
//        http.logout().logoutSuccessUrl("/");
//        //开启记住我功能，cookie默认保存两周,自定义接收前端参数
////        http.rememberMe().rememberMeParameter("remember");
//
//    }
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/PearAdmin/**");      //放行静态资源
////        web.ignoring().mvcMatchers("/captcha","/login");         //排除登录拦截captcha路径
//    }
//
//
//}
