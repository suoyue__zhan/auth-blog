package com.blog.enums;

import lombok.Data;

/**
 * @author suoyue_zhan
 * @date 2021/04/08 18:12
 * @descript 静态变量类
 */
@Data
public class StaticVariable {

    //邮件发送者
    public static String receiverMail = "1665741813@qq.com";

    //默认用户头像
    public static String avatarImg = "/images/avatar.jpg";

    //普通用户类型为：1
    public static Integer userType = 1;

    //管理员类型为：2
    public static Integer adminType = 2;

    //上传头像的目录
    public static String uploadAvatar = "/upload/avatar/";


    /**
     * redis 键值
     */
    //文章访问量
    public static String blogNum = "blog_num: ";
    //文章详细信息
    public static String blogDetail = "blog_detail: ";
    //文章评论
    public static String blogComment = "blog_comment: ";


}
