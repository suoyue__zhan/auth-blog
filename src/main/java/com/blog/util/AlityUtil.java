package com.blog.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author suoyue_zhan
 * @date 2021/02/09 21:37
 * @descript 类型转换
 */
public class AlityUtil {

    /**
     * Formate: yyyy-MM-dd
     * 系统当前时间
     * @return
     */
    public String nowDataYMD(){
        long timeMillis = System.currentTimeMillis();
        Date date = new Date(timeMillis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }

    /**
     * Formate: yyyy-MM-dd
     * 6天前的当前系统时间
     * @return
     */
    public String sixDataBeforeYMD(){
        long timeMillis = System.currentTimeMillis();
        timeMillis -= 6*24*60*60*1000;
        Date date = new Date(timeMillis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }

    public static void main(String[] args) {

    }


}
