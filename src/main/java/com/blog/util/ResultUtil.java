package com.blog.util;

import com.blog.enums.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author suoyue_zhan
 * @date 2021/02/04 10:39
 * @descript
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResultUtil<T> {
    /**
     * 1.status状态值：代表本次请求response的状态结果。
     */
    private Integer status;
    /**
     * 2.response描述：对本次状态码的描述。
     */
    private String desc;
    /**
     * 3.data数据：本次返回的数据。
     */
    private T data;

    /**
     * 成功，创建ResResultUtil：没data数据
     */
    public ResultUtil success() {
        ResultUtil resultUtil = new ResultUtil();
        resultUtil.setResultUtilCode(ResultCode.SUCCESS);
        return resultUtil;
    }

    /**
     * 成功，创建ResResultUtil：有data数据
     */
    public ResultUtil success(Object data) {
        ResultUtil resultUtil = new ResultUtil();
        resultUtil.setResultUtilCode(ResultCode.SUCCESS);
        resultUtil.setData(data);
        return resultUtil;
    }

    /**
     * 失败，指定status、desc
     */
    public ResultUtil failure(Integer status, String desc) {
        ResultUtil resultUtil = new ResultUtil();
        resultUtil.setStatus(status);
        resultUtil.setDesc(desc);
        return resultUtil;
    }

    /**
     * 失败，指定ResultUtilCode枚举
     */
    public ResultUtil failure(ResultCode ResultCode) {
        ResultUtil resultUtil = new ResultUtil();
        resultUtil.setResultUtilCode(ResultCode);
        return resultUtil;
    }

    /**
     * 把ResultUtilCode枚举转换为ResResultUtil
     */
    private void setResultUtilCode(ResultCode code) {
        this.status = code.code();
        this.desc = code.message();
    }


}
