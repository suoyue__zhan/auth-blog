package com.blog.service;

import com.blog.entity.User;

import java.util.List;

/**
 * 用户业务层接口
 */
public interface UserService {

    User checkUser(String username,String password);


    User checkUserByEmailAndPassword(String email,String password);

    //检查邮箱并发送邮箱验证码
    int checkEmailCode(String email);

    //检验邮件验证码是否有效并注册用户信息
    int checkSignInInfo(User user,String code);

    //检验邮箱及邮件验证码是否可登录
    int checkEmailAndCode(String email,String code);

    //通过邮箱查找用户信息
    User selectUserByEmail(String email);

    //修改用户信息
    int updateUserInfo(User user);

    //查找可用用户基本信息列表
    List<User> selectUserInfo();

    //查询首页用户信息列表
    List<User> selectFirstPageUser();

    //查找全部用户信息列表
    List<User> selectUserList();

    //查找用户基本信息
    User selectUserDetailInfo(Long id);

    //动态（多条件）用户信息列表
    List<User> selectDynamicUserInfo(User user);

    //动态（多条件-有boolean值）用户信息列表
    List<User> selectBooleanUserInfo(User user);
}
