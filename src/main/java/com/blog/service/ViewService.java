package com.blog.service;

import com.blog.entity.View;

import java.util.List;

/**
 * @author suoyue_zhan
 * @date 2021/02/08 12:52
 * @descript 访问量接口
 */
public interface ViewService {

    //查询日期范围的数据
    List<View> selectViewsByDate(String dateFrom, String dateTo);
    //插入访问量数据
    Integer insertView(View view);

}
