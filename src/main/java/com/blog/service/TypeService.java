package com.blog.service;

import com.blog.entity.Type;

import java.util.List;


/**
 * 文章分类业务层接口
 */
public interface TypeService {

    int saveType(Type type);

    //查询分类页面
    Type getType(Long id);

    //获取用户对应的分类
    List<Type> getUserType(Long userId);

    List<Type> getAllTypeAndBlog();

    //查询用户拥有的分类信息
    List<Type> getUserTypeAndBlog(Long userId);

    //根据用户id判断分类是否存在
    Type getTypeByNameAndUserId(String name,Long userId);
    //根据分类id判断分类是否存在
    Type getTypeByNameAndId(String name,Long id);

    //修改分类信息
    int updateType(Type type);

    void deleteType(Long id,Long userId);

}
