package com.blog.service;

import com.blog.entity.Image;

import java.util.List;

/**
 * @Author zqc
 * @Date 2021/1/18-9:52
 * @Description 博客图片列表业务层接口
 */
public interface PictureService {

    //查询用户所有图片信息
    List<Image> selectPictureList(Long userId);

    //插入图片信息
    int insertPicture(Image image);

    int deleteImageInfoById(Long id);       //删除图片信息

    //设置图片信息不可用
    int updateImageValid(Long id,Long userId);

    //查询图片信息
    Image selectImageInfo(Long id);

    int updateImageInfo(Image image);       //修改图片信息

    //查找图片链接
    String selectURLById(Long id,Long userId);

}
