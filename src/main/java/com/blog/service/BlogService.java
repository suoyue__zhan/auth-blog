package com.blog.service;

import com.blog.entity.Blog;
import com.blog.vo.*;

import java.util.List;

/**
 * 博客列表业务层接口
 */
public interface BlogService {

    ShowBlog getBlogById(Long id,Long userId);

    //查询所有文章列表
    List<Blog> getAllBlog();

    //管理员的文章搜索
    List<Blog> getBlogByAdminSearch(String title, Long userId);

    //获取用户的文章列表
    List<BlogQuery> getUserBlog(Long userId);

    //设置文章可见性－用户删除文件
    int updateBlogView(Long id, Long userId);

    //动态更新文章信息
    int updateBlogInfo(Blog blog);

    //时间轴－最新文章列表
    List<BlogQuery> getArchiveBlog();

    int saveBlog(Blog blog);

    int updateBlog(ShowBlog showBlog);

    void deleteBlog(Long id);

    //用户的文章搜索
    List<BlogQuery> getBlogBySearch(SearchBlog searchBlog);

    //热门文章列表
    List<FirstPageBlog> getAllFirstPageBlog();

    //首页的热门文章
    List<FirstPageBlog> getHotFirstPageBlog();

    //首页的最新文章－根据updateTime排序
    List<FirstPageBlog> getNewFirstPageBlog();

    List<RecommendBlog> getRecommendedBlog();

    List<FirstPageBlog> getSearchBlog(String query);

    DetailedBlog getDetailedBlog(Long id);

    //进redis
    List<DetailedBlog> getAllDetailedBlog();

    //根据TypeId获取博客列表，在分类页进行的操作
    List<FirstPageBlog> getByTypeId(Long typeId);

    int updateView(Long id,Integer views);

    Integer getBlogTotal();

    Integer getBlogViewTotal();

    Integer getBlogCommentTotal();

    Integer getBlogMessageTotal();

}