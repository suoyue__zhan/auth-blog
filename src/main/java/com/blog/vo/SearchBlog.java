package com.blog.vo;

import lombok.Data;

/**
 * 搜索博客管理列表
 */
@Data
public class SearchBlog {

    private String title;
    private Long typeId;
    private boolean recommend;

    //用户ID
    private Long userId;

}