package com.blog.vo;

import lombok.Data;

import java.util.Date;

/**
 * 博客详情
 */
@Data
public class DetailedBlog {

    private Long id;
    private Long userId;
    private String firstPicture;
    private String flag;
    private String title;
    private String content;
    private Integer views;
    private Integer commentCount;
    private Date updateTime;
    private boolean commentabled;
    private boolean shareStatement;
    private boolean appreciation;

    private String nickname;
    private String username;
    private String avatar;

    private String typeName;

}