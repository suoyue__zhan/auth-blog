package com.blog.vo;

import lombok.Data;

import java.util.Date;

/**
 * 博客首页数据实体类
 */
@Data
public class FirstPageBlog {

    //Blog
    private Long id;
    private String title;
    private String firstPicture;
    private Integer views;
    private Integer commentCount;
    private Date updateTime;
    private String description;
    private Long typeId;
    private Long userId;

    //Type
    private String typeName;

    //User
    private String username;
    private String avatar;


}