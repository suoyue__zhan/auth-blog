package com.blog.vo;

import com.blog.entity.Type;
import lombok.Data;

import java.util.Date;

/**
 * 显示数据实体类
 */
@Data
public class BlogQuery {

    private Long id;
    private String title;
    private Date createTime;
    private Date updateTime;
    private Boolean recommend;
    private Boolean published;
    private Long typeId;
    private Type type;

    //true-可见 false－不可见
    private boolean appreciation;

}