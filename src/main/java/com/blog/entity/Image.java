package com.blog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 照片实体类
 */
@Data
public class Image {

    private Long id;
    private String url;         //图片链接
    //true-可用 false－不可用
    private boolean valid;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;    //图片创建/上传时间

    //用户Id
    private Long userId;
}