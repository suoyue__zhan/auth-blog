package com.blog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author suoyue_zhan
 * @date 2021/02/08 10:44
 * @descript 日访问量实体类
 */
@Data
public class View {

    private int id;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    private Date dateTime;
    private int view;

    public View(Date dateTime, int view) {
        this.dateTime = dateTime;
        this.view = view;
    }

    public View(int id, Date dateTime, int view) {
        this.id = id;
        this.dateTime = dateTime;
        this.view = view;
    }

}
