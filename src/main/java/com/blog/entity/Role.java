package com.blog.entity;

import lombok.Data;

/**
 * 用户角色表
 */
@Data
public class Role {

    private Long id;
    private String name;

}
