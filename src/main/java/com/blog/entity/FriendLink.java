package com.blog.entity;

import java.util.Date;

/**
 * 友链实体类
 */
public class FriendLink {

    private Long id;
    private int uid;
    private String blogname;
    private String blogaddress;
    private String pictureaddress;
    private Date createTime;

    public FriendLink() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBlogname() {
        return blogname;
    }

    public void setBlogname(String blogname) {
        this.blogname = blogname;
    }

    public String getBlogaddress() {
        return blogaddress;
    }

    public void setBlogaddress(String blogaddress) {
        this.blogaddress = blogaddress;
    }

    public String getPictureaddress() {
        return pictureaddress;
    }

    public void setPictureaddress(String pictureaddress) {
        this.pictureaddress = pictureaddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public FriendLink(Long id, int uid, String blogname, String blogaddress, String pictureaddress, Date createTime) {
        this.id = id;
        this.uid = uid;
        this.blogname = blogname;
        this.blogaddress = blogaddress;
        this.pictureaddress = pictureaddress;
        this.createTime = createTime;
    }
}