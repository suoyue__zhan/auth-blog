package com.blog.entity;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * 分类实体类
 */
@Data
public class Type {

    private Long id;
    @NotBlank(message = "分类名称不能为空")     //Controller层需要使用@Valid配合使用
    private String name;

    //用户ID
    private Long userId;

    private List<Blog> blogs = new ArrayList<>();


}