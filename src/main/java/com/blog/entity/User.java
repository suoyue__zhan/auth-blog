package com.blog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 用户实体类
 * 实现Security内置的UserDetails
 * Security可以通过SecurityContextHolder取得用户凭证和用户所有的信息，用户信息都存放在系统内置的UserDetails中
 */
@Data
//public class User implements UserDetails {
public class User {

    private Long id;
    private String username;
    private String password;
    private String nickname;
    private String email;
    private String avatar;

    //1-普通用户    2-管理员
    private Integer type;
    //true-正常可用 false-禁用
    private boolean enabled;
    //true－好友推荐 false－默认
    private boolean recommend;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private Date updateTime;

//    private List<Role> roles;

    private String confirmPassword;

//    @Override
//    @JsonIgnore
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        List<GrantedAuthority> authorities = new ArrayList<>();
//        for (Role role : roles) {
//            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
//        }
//        return authorities;
//    }
//
//    @Override
//    @JsonIgnore
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Override
//    @JsonIgnore
//    public boolean isAccountNonLocked() {
//        return true;
//    }
//
//    @Override
//    @JsonIgnore
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return enabled;
//    }
}