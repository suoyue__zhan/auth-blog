package com.blog;

import com.blog.entity.User;

/**
 * @author suoyue_zhan
 * @date 2021/04/15 16:10
 * @descript
 */
public class EasyTest {

    public static void main(String[] args) {
        User user1 = new User();
        user1.setUsername("admin");

        User user2 = new User();
        user2.setUsername("admin");

        if(user1 == user2){
            System.out.println("==");
        }else {
            System.out.println("!=");
        }
    }

}
