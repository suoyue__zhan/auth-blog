package com.blog;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @Author zqc
 * @Date 2020/11/29-10:19
 * @Description 使用Jedis测试与redis的连通性
 */
public class JedisPoolApp {

    public static final String HOST = "IP";

    public static void main(String[] args) {
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        //配置连接池的相关信息
        poolConfig.setMaxTotal(8);          //连接池最大连接数（使用负值表示没有限制）
        poolConfig.setMaxIdle(8);           //连接池中的最大空闲连接
        poolConfig.setMinIdle(0);           //连接池中的最小空闲连接
        poolConfig.setMaxWaitMillis(5000);  //连接池最大阻塞等待时间（使用负值表示没有限制）
        //此处若无设置密码，可使用IP和端口直接建立连接；注意redis的配置文件需要修改 bind 0.0.0.0或指定特定可连接IP
        JedisPool jedisPool = new JedisPool(poolConfig,"IP",6379,2000,"password");
        //从连接池里面得到连接
        Jedis jedis = jedisPool.getResource();
//        jedis.auth("password");       //若不用连接池也可直接new创建连接，设置密码的另一种方式
        System.out.println("jedis："+jedis);
        //ping一下，测试连通性
        String ping = jedis.ping();
        System.out.println("ping："+ping);
        //使用 jedis. 进行redis命令对应方法的调用
        jedis.set("jedis","suoyue_詹");
        System.out.println(jedis.get("jedis"));
        jedis.close();

        /**
         * 集群配置，使用JedisCluster
         */
//        Set<HostAndPort> nodes = new HashSet<>();
//        nodes.add(new HostAndPort(HOST, 7000));
//        nodes.add(new HostAndPort(HOST, 7001));
//        nodes.add(new HostAndPort(HOST, 7002));
//        nodes.add(new HostAndPort(HOST, 7003));
//        nodes.add(new HostAndPort(HOST, 7004));
//        nodes.add(new HostAndPort(HOST, 7005));
//        JedisCluster jedisCluster = new JedisCluster(nodes);

    }
}
