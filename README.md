
## 社交网站-博客系统-V3.0（SpringBoot+Mybatis+Redis）

博客地址： **http://suoyue.top:8080/** 

演示视频：[B站成果视频演示](https://www.bilibili.com/video/BV1PU4y1b78a)

- 旧版：[Blog-V1.0](https://gitee.com/suoyue__zhan/my-blog)
- 旧版：[Blog-V2.0](https://gitee.com/suoyue__zhan/my-blog2)

持久层基于JPA的教学视频：[https://www.bilibili.com/video/BV1nE411r7TF?t=1514&p=40](https://www.bilibili.com/video/BV1nE411r7TF?t=1514&p=40)

视频原作者：[https://edu.csdn.net/course/detail/6359?utm_source=edu_bbs_autocreate](https://edu.csdn.net/course/detail/6359?utm_source=edu_bbs_autocreate)

参考博客：[https://onestar.newstar.net.cn/](https://onestar.newstar.net.cn/)


### 前言 - 导出项目步骤
 1. 克隆项目代码。
 - 使用git克隆项目到开发工具：IDEA（IDEA需安装Lombok插件，推荐安装Free Mybatis plugin插件，建议配置maven和git）
 2. 建库建表。 
 - 将myBlog.sql文件导入mysql数据库中（复制建库建表语句直接运行也可以）
 3. 修改配置文件：src/main/resources/application-dev.yml
 - 修改数据库连接帐号密码；
 - 项目用到redis，也需要配置一下redis的连接信息（IP、密码），否则用到redis的业务会导致页面无法显示，可在service层将用到的redis代码注释掉，此处注意不要把查询数据库的代码也注释了
 4. 修改邮件配置文件
 - src/main/java/com/blog/enums/StaticVariable.java：修改用来发送邮件的QQ邮箱;
 -  src/main/java/com/blog/util/MailUtil.java：54行处修改QQ邮箱的授权码（53行有授权码的获取方式）

 5. 启动项目


### 一、技术栈

#### 1.前端
- JS框架：JQuery
- CSS框架：[Semantic UI官网](https://semantic-ui.com/)
- Markdown编辑器：[编辑器 Markdown](https://github.com/pandao/editor.md)
- 代码高亮：[代码高亮 prism](https://github.com/PrismJS/prism)
- 动画效果：[动画 animate.css](https://daneden.github.io/animate.css/)
- 文章目录：[目录生成 Tocbot](https://tscanlin.github.io/tocbot/)
- 中文网页重设与排版：[typo.css](https://github.com/sofish/typo.css/blob/master/README.md)
- 平滑滚动：[scrollTo](https://github.com/flesler/jquery.scrollTo)
- 滚动侦测：[waypoints](http://imakewebthings.com/waypoints/)
- 看板娘：[live2d](https://github.com/stevenjoezhang/live2d-widget)

#### 2.后端
- 核心框架：SpringBoot 2.2.5
- 项目构建：jdk1.8、Maven 3
- 持久层框架：Mybatis
- swagger规范：[Swagger](https://swagger.io/tools/swagger-ui/)
- 分布式文件系统：[fastDFS](https://github.com/happyfish100/fastdfs)
- 模板框架：[Thymeleaf](https://www.thymeleaf.org/)
- 分页插件：[PageHelper](https://github.com/pagehelper/Mybatis-PageHelper/blob/master/wikis/zh/HowToUse.md)
- 加密：MD5加密
- 图形验证码：[EasyCaptcha](https://gitee.com/whvse/EasyCaptcha)
- Markdown格式转换：[Markdown](https://github.com/atlassian/commonmark-java)
- 邮件发送sdk：javax mail

#### 3.数据库
- MySQL 5.7
- Redis 5.0.8

#### 4.项目部署
- 运行环境：[阿里云Centos7-①新用户免费1个月②学生认证免费1个月，完成课程再加一个月。注意无法叠加成3个月，只能二选一](https://developer.aliyun.com/plan/grow-up?spm=a2c6h.12873581.1364563.37.1a1670afKXISpO)
- Docker：[服务器购买/搭建注意点+项目部署上线](https://blog.csdn.net/suoyue_py/article/details/109535339)
- nginx：动态代理


### 二、功能需求
系统根据用户身份划分权限，可分成三大模块
- 游客角色：只有前台页面信息的浏览功能
- 用户：则有属于自己的创作中心，可进行信息修改、图片上传、内容的输出与评论
- 管理员：身份则主要负责用户的权限管理与用户的内容输出管理。

![社交网站设计与实现架构图](https://images.gitee.com/uploads/images/2021/0604/143933_18850f6e_7888604.png "image.png")

#### 1.游客
- 首页：登录、注册、查看推荐用户、查看部分最热门文章、查看部分最新文章
- 查看文章信息：文章发布者、标题、内容、发布时间、访问量以及评论等信息
- 查看好友信息：好友列表、好友头像、好友名、好友邮箱、好友的分类文章信息
- 查看热门文章：按照文章访问量查看文章信息
- 查看时间轴：按照文章发布时间查看文章信息
- 交流区：查看用户的留言信息
- 搜索文章：导航栏右侧搜索框可根据关键字进行模糊匹配

![输入图片说明](https://images.gitee.com/uploads/images/2021/0604/144307_bf625806_7888604.png "1.png")


#### 2.用户
- 拥有游客的所有功能权限
- 登录：可根据数据库的邮箱和密码进行登录，也可采用邮箱获取验证码直接登录
- 个人信息管理：管理个人头像、名称、修改密码
- 文章模块管理：查询文章信息列表、新增文章、编辑文章、删除文章、搜索文章
- 分类模块管理：查询分类标签列表、新增分类、删除分类、修改分类
- 消息模块管理：登录后才可进行评论、留言，可点击删除按键删除自身留言的信息
- 图片模块管理：查看图片列表、上传图片、删除图片

![输入图片说明](https://images.gitee.com/uploads/images/2021/0604/144240_8cf12998_7888604.png "2.png")


#### 3.管理员（栈主）
- 拥有游客、普通用户的所有功能权限
- 登录：主页路径下加“/admin”，可进入登录页面，根据数据库用户名和密码进行登录
- 用户管理：查询用户列表、限制用户权限，对违规用户进行禁用处理，解除禁用
- 文章管理：查询文章列表、设置文章不可见、恢复文章可见度、删除文章

![输入图片说明](https://images.gitee.com/uploads/images/2021/0604/144205_cfd8b2c1_7888604.png "3.png")


### 三、数据库设计

#### 1.数据表
- 博客数据表：t_blog
- 分类数据表：t_type
- 用户数据表：t_user
- 评论数据表：t_comment
- 留言数据表：t_message
- 图片数据表：t_picture
- 访问数据表：t_view

#### 2.实体关系

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/105442_3533523e_7888604.png "屏幕截图.png")

- 博客和分类是多对一的关系：一个博客对应一个分类，一个分类可以对应多个博客
- 博客和用户是多对一的关系：一个博客对应一个用户，一个用户可以对应多个博客
- 博客和评论是一对多的关系：一个博客可以对应多个评论，一个评论对应一个博客
- 评论和回复是一对多的关系：一个评论可以对应多个回复，一个回复对应一个评论
> 留言和评论是一样的，友链数据表和其他表没有关联

#### 3.实体属性

- 博客属性：标题、内容、首图、标记、浏览次数、赞赏开启、版权开启、评论开启、是否发布、创建时间、更新时间、描述

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/105549_fe570871_7888604.png "屏幕截图.png")

- 分类属性：分类名称

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/105559_0555911c_7888604.png "屏幕截图.png")

- 用户属性：昵称、用户名、密码、邮箱、类型、头像、创建时间、更新时间

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/105609_efe4e2f8_7888604.png "屏幕截图.png")

- 评论属性：昵称、邮箱、头像、评论内容、创建时间、博客id、父评论id、管理员id

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/105617_bf0410c7_7888604.png "屏幕截图.png")

- 留言属性：昵称、邮箱、头像、留言内容、创建时间、父留言id、管理员id

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/105624_1e60271a_7888604.png "屏幕截图.png")

- 友链属性：网址、名称、创建时间、图片地址

![输入图片说明](https://images.gitee.com/uploads/images/2020/1007/110412_6105db8d_7888604.png "屏幕截图.png")

- 图片属性：图片链接、图片类型、图片创建时间


#### 4.表结构

![PowerDesigner](https://images.gitee.com/uploads/images/2021/0604/144406_00ea1a3a_7888604.png "image (1).png")

#### 4.建表语句

```
/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.24 : Database - myblog
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`myblog` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `myblog`;

/*Table structure for table `t_blog` */

DROP TABLE IF EXISTS `t_blog`;

CREATE TABLE `t_blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appreciation` bit(1) NOT NULL,
  `commentabled` bit(1) NOT NULL,
  `content` longtext,
  `create_time` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `first_picture` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `published` bit(1) NOT NULL,
  `recommend` bit(1) NOT NULL,
  `share_statement` bit(1) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `comment_count` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK292449gwg5yf7ocdlmswv9w4j` (`type_id`) USING BTREE,
  KEY `FK8ky5rrsxh01nkhctmo7d48p82` (`user_id`) USING BTREE,
  CONSTRAINT `FK292449gwg5yf7ocdlmswv9w4j` FOREIGN KEY (`type_id`) REFERENCES `t_type` (`id`),
  CONSTRAINT `FK8ky5rrsxh01nkhctmo7d48p82` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_blog` */

/*Table structure for table `t_comment` */

DROP TABLE IF EXISTS `t_comment`;

CREATE TABLE `t_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `blog_id` bigint(20) DEFAULT NULL,
  `parent_comment_id` bigint(20) DEFAULT NULL,
  `admin_comment` bit(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_comment` */

/*Table structure for table `t_friend` */

DROP TABLE IF EXISTS `t_friend`;

CREATE TABLE `t_friend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` int(2) NOT NULL COMMENT '0-自荐网址;1-友链',
  `blogaddress` varchar(255) NOT NULL,
  `blogname` varchar(255) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `pictureaddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_friend` */

/*Table structure for table `t_message` */

DROP TABLE IF EXISTS `t_message`;

CREATE TABLE `t_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `parent_message_id` bigint(20) DEFAULT NULL,
  `admin_message` bit(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_message` */

/*Table structure for table `t_type` */

DROP TABLE IF EXISTS `t_type`;

CREATE TABLE `t_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_type` */

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_user` */

/*Table structure for table `t_picture` */

DROP TABLE IF EXISTS `t_picture`;

CREATE TABLE `t_picture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `is_valid` bit(1) NOT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_picture` */

/*Table structure for table `t_view` */

DROP TABLE IF EXISTS `t_view`;

CREATE TABLE `t_view` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL COMMENT '日期',
  `view` int(11) NOT NULL COMMENT '日访问量',
  PRIMARY KEY (`id`),
  KEY `date_time` (`date_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_view` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

```

